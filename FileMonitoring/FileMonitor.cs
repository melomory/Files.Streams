﻿using FileMonitoring.Interfaces;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace FileMonitoring
{
    public class FileMonitor : IFileMonitor
	{
		private readonly string _trackingPath;
		private readonly string _backupPath;
		private FileSystemWatcher _fileSystemWatcher;
		private bool _disposed = false;
		private readonly string fileNameFilter = "*.txt";
		private readonly string dateTimeFormatForBackupName = "dd_MM_yyyy_HH_mm_ss";

		public FileMonitor(IConfiguration configuration)
		{
			_trackingPath = configuration.Path;
			_backupPath = configuration.BackupPath;
		}

		public void Start()
		{
			ConfigureFileSystemWatcher();
			MakeBackup();
		}

		public void Stop()
		{
			StopWatching();

			_fileSystemWatcher.Dispose();
			_fileSystemWatcher = null;

			CleanUp(_backupPath);
		}

		public void Reset(DateTime onDateTime)
		{
			string backupDirectoryNameToRestoreFrom = Directory
				.GetDirectories(_backupPath)
				.Where
				(
					directoryName => DateTime.ParseExact
					(
						Path.GetFileName(directoryName), 
						dateTimeFormatForBackupName, 
						CultureInfo.CurrentCulture
					) <= onDateTime
				)
				.Max();

			StopWatching();
			
            string[] files = Directory
				.GetFiles(_trackingPath)
				.Where(fileName => Regex.IsMatch(fileName, $".{fileNameFilter}$"))
				.ToArray();

            foreach (string file in files)
            {
				File.Delete(file);           
            }

            CopyFiles(backupDirectoryNameToRestoreFrom, _trackingPath);

			StartWatching();
		}

		private void StartWatching()
        {
			_fileSystemWatcher.Changed += OnChanged;
			_fileSystemWatcher.Created += OnCreated;
			_fileSystemWatcher.Deleted += OnDeleted;
			_fileSystemWatcher.Renamed += OnRenamed;
		}

		private void StopWatching()
		{
			_fileSystemWatcher.Changed -= OnChanged;
			_fileSystemWatcher.Created -= OnCreated;
			_fileSystemWatcher.Deleted -= OnDeleted;
			_fileSystemWatcher.Renamed -= OnRenamed;
		}

		private void MakeBackup()
		{
			DateTime backupTime = DateTime.Now;
            DirectoryInfo backupDirectory = Directory.CreateDirectory(_backupPath);
            DirectoryInfo currentBackupDirectory = backupDirectory.CreateSubdirectory(backupTime.ToString(dateTimeFormatForBackupName));
			CopyFiles(_trackingPath, currentBackupDirectory.FullName);
		}

		private void CopyFiles(string sourcePath, string destinationPath)
        {
			if (string.IsNullOrEmpty(sourcePath) || string.IsNullOrEmpty(destinationPath))
            {
				return;
            }

			string[] files = Directory
				.GetFiles(sourcePath)
				.Where(fileName => Regex.IsMatch(fileName, $".{fileNameFilter}$"))
				.ToArray();

			foreach (string file in files)
			{
				string sourceFileName = Path.GetFileName(file);
				string destFilePathName = Path.Combine(destinationPath, sourceFileName);
				if (File.Exists(file))
                {
					File.Copy(file, destFilePathName, true);
				}							
			}
		}

		private void CleanUp(string path)
        {
			Directory.Delete(path, true);
		}

		private void ConfigureFileSystemWatcher()
        {
			_fileSystemWatcher = new FileSystemWatcher(_trackingPath, fileNameFilter);

			_fileSystemWatcher.NotifyFilter  = NotifyFilters.Attributes
											| NotifyFilters.CreationTime
											| NotifyFilters.DirectoryName
											| NotifyFilters.FileName
											| NotifyFilters.LastAccess
											| NotifyFilters.LastWrite
											| NotifyFilters.Security
											| NotifyFilters.Size;

			StartWatching();

			_fileSystemWatcher.IncludeSubdirectories = true;
			_fileSystemWatcher.EnableRaisingEvents = true;
		}

        private void OnRenamed(object sender, RenamedEventArgs e)
        {
			MakeBackup();
		}

        private void OnDeleted(object sender, FileSystemEventArgs e)
        {
			MakeBackup();
		}

        private void OnCreated(object sender, FileSystemEventArgs e)
        {
			MakeBackup();
		}

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
			MakeBackup();
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				if (disposing)
				{
					Stop();
				}
				_disposed = true;
			}
		}
	}
}
